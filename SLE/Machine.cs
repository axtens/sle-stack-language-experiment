﻿/*
 * Created by SharpDevelop.
 * User: BruceAxtens
 * Date: 8/06/2013
 * Time: 6:34 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace SLE
{
    /// <summary>
    /// Description of Machine.
    /// </summary>
    
    public class Machine
    {
        private int ip;
        private bool flagbugs;
        private bool testflag;
        private Stack<object> DStack = new Stack<object>();
        private Stack<object> CStack = new Stack<object>();
        private Dictionary<string,object> vars = new Dictionary<string, object>();
        private string[] Script = null;
        
        public Machine()
        {
            testflag = false;
            flagbugs = false;
            ip = 0;	
        }
        private void BUG(string item)
        {
            if (flagbugs)
            {
                Console.Write(item.ToString());
            }
        }
        
        private void BUGL(string item)
        {
            if (flagbugs)
            {
                Console.WriteLine(item.ToString());
            }
        }

        public void LOAD(string fileName, params string[] args)
        {
            Script = File.ReadAllLines(fileName);
            CPUSH(Script.Length + 1);
            foreach( string arg in args)
            {
                DPUSH(arg);
            }		
        }
        
        private void CPUSH(int item)
        {
            CStack.Push(item);
        }
        
        private void DPUSH(string item)
        {	
            int temp = 0;
            if ( int.TryParse(item,out temp) )
            {
                DStack.Push(new BigInteger(temp));
            }
            else
            {
                string vname = VARNAME(item);
                if (vars.ContainsKey(vname)) 
                {
                    DStack.Push((vars[vname]));
                }
                else
                {
                    //not sure what to do with non numeric that's not a var name
                }
            }
        }
        
        private string VARNAME(string item)
        {
            return item.ToString() + "!" + CStack.Count.ToString();
        }

        private string CODE()
        {
            return Script[ip];
        }
        
        public void DUMPCODE()
        {
            Console.WriteLine(String.Join("\n",Script));
        }
    
        public void DUMPSTACKS()
        {
            Console.WriteLine( "[" + String.Join(",",DStack.ToArray()) + "]\n[" + String.Join(",",CStack.ToArray()) + "]");
        }

        private void NEXTIP()
        {
            ip++;
        }
        
        private int CURRIP()
        {
            return ip;
        }
        
        private bool ENDS()
        {
            return ip > Script.Length;
        }
        
        private void DPOP(string X)
        {
            string var = VARNAME(X.ToString());
            vars[var] = DStack.Pop();
        }
        
        private void CPOP(out object x)
        {
            x = CStack.Pop();
        }
        private void ICR( string x )
        {
            string var = VARNAME(x);
            vars[var] = ((BigInteger)(vars[var]) + 1);
        }

        private void DCR( string x )
        {
            string var = VARNAME(x.ToString());
            vars[var] = ((BigInteger)(vars[var]) - 1);
        }
        
        private void JMPR(object x)
        {
            ip += (int)x;
        }
        
        private void JMPA(object x)
        {
            ip = (int)x;
        }

        private void JMPS()
        {
            ip = 0;
        }
        
        private void OUT(string x)
        {
            BUG("" + (char)int.Parse(x));
        }

        private void ADD(string x, string y)
        {
            int temp = int.Parse(x) + int.Parse(y);
            x = temp.ToString();
            return;
        }
        private void CPUSHIP(object offset)
        {
            //int temp = int.Parse(offset);
            CStack.Push(ip + (int)offset);
            return;
        }
        private void QUIT()
        {
            ip = (int)(Script.Length + 1);
            return;
        }
        private void ISZERO(string x)
        {
            BigInteger res;
            string var = VARNAME(x);
            if (vars.ContainsKey(var))
            {
                res = (BigInteger)vars[var];
                testflag = res == 0;
            }
        }
        private void JTR(object x)
        {
            if (testflag)
            {
                JMPR((int)x);
            }
        }
        private void JTA(object x)
        {
            if (testflag)
            {
                JMPA((int)x);
            }
        }
        private void JFR(object x)
        {
            if (!testflag)
            {
                JMPR((int)x);
            }
        }
        private void JFA(object x)
        {
            if (!testflag)
            {
                JMPA((int)x);
            }
        }
        private void MOVE(out string x, string y)
        {
            x = y;
            return;
        }
        
        private int SCRIPTLEN()
        {
            return Script.Length;
        }
        
        private void BSR(int n)
        {
            CPUSHIP(0);
            //int temp = int.Parse(n) - 1;
            JMPA(n-1);
            return;
        }
        
        private void RET()
        {
            object r;
            CPOP(out r);
            JMPA(r);
            return;
        }
        private void BUGS(string n)
        {
            if (int.Parse(n)==1)
                flagbugs = true;
            else
                flagbugs = false;
            return;
        }
        //public void RUN(string fileName, params string[] args)
        //{
        //	
        //}
        
        public string DTOP()
        {
            BigInteger i = (BigInteger)DStack.Pop();
            return i.ToString();
        }
        
        public void LISTVARS()
        {
            Console.WriteLine( String.Join(",", vars.Keys ));
        }
        

        public void EXECUTE()
        {
            string lastcode = "";
            for (;;)
            {
                lastcode = CODE();
                if (lastcode.Substring(0,1) != "'")
                {
                    char[] delimiters = new char[] { ' ' };
                    string[] aCode = lastcode.Split(delimiters, 2);
                    if (aCode.Length>1) 
                        aCode[1] = aCode[1].Trim();
                    
                
                    switch (aCode[0].ToUpper())
                    {
                    case "BUGS":
                        BUGS(aCode[1]);
                        break;
                    case "DPOP":
                        DPOP(aCode[1]);
                        break;
                    case "ISZERO":
                        ISZERO(aCode[1]);
                        break;
                    case "ICR":
                        ICR(aCode[1]);
                        break;
                    case "BSR":
                        BSR(int.Parse(aCode[1]));
                        break;
                    case "DCR":
                        DCR(aCode[1]);
                        break;
                    case "DPUSH":
                        DPUSH(aCode[1]);
                        break;
                    case "RET":
                        RET();
                        break;
                    case "JFR":
                        JFR(int.Parse(aCode[1]));
                        break;
                    default:
                        break;
                    }
                    
                }
                NEXTIP();
                if (ENDS()) break;
            }			
        }
    }
}
/*

    sub process( s )
        dim aCode
        aCode = split( s , " " )
        '~ bug " elements in aCode=" & ubound(aCode)+1 & " "
        bug  "{" & join( aCode, "|" ) & "} "
        select case ubound(aCode)
        case 0
            '~ bug " single opcode "
            execute aCode(0)  
        case else
            '~ bug " opcode with arg "
            execute aCode(0) & chr(34) & aCode(1) & chr(34) 
        end select
    end sub
    
    function listvars()
        listvars = ovar.varlist
    end function
    
    function run( sFile, aArgs )
        dim i
        dim lastcode
        dim res
        script = ofso.opentextfile( sFile ).readall
        script = split( script, vbnewline )
        cstk.push scriptlen + 1
        for i = 0 to ubound( aArgs )
            dstk.push aArgs(i)
        next
        do
            lastcode = rtrim(code)
            bug " " & lastcode & " --> "
            if left(code,1) <> "'" then process code
            bugl
            nextip
            if ends then exit do
        loop
        RUN = dstk.pop
    end function
    
end class 

*/