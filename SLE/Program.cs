﻿/*
 * Created by SharpDevelop.
 * User: BruceAxtens
 * Date: 8/06/2013
 * Time: 6:01 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace SLE
{
    
    class Program
    {
        
        public static void Main(string[] args)
        {
            for (var i = 0; i <= 3; i++ ) 
            {
                for (var j = 0; j <= 10; j++)
                {
                    Machine box = new Machine();
                    box.LOAD(@"..\..\test.ia", i.ToString(), j.ToString());
                    box.EXECUTE();
                    Console.WriteLine("Ackermann({0},{1})={2}", i, j, box.DTOP());
                    //box.LISTVARS();
                    //box.DUMPSTACKS();
                    //box.DUMPCODE();
            
                }
            }
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}

/*
dim box

dim res
dim lastcode
dim i, j
for i = 0 to 4
    for j = 0 to 6
        set box = new machine
        res = box.run( "acker.ia", array( i, j ) )
        wscript.echo "ackermann(",i,j,")=", res
        res = box.listvars
        wscript.echo ubound( res(0) ),"stack levels used"
        set box = nothing
    next
next
*/